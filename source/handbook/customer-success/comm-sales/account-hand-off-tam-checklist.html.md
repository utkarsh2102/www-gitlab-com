---
layout: markdown_page
title: "Account Hand-Off TAM-to-TAM Checklist"
---

## Account Hand-Off TAM-to-TAM Checklist

Below are checkpoints during the account hand-off process that TAMs can use to keep track of information they will need in order to successfully transition accounts


### License Evaluation

Sufficient knowledge of the state of the customer's license

* Active users count
* True-up assessment
* Upcoming renewal

### Ticket Evaluation

Thorough understanding of the main technical issues the customer cares about

* Take note of critical open tickets and collaborate with support
* Review key technical issues the customer experienced in the past (i.e Migrations, Integrations, major upgrades)

### Communication Evaluation

Thorough understanding of the communication channels used to interact with the customer

* Most recent correspondence
* Past critical conversations (i.e Slack threads, Chorus playlist, Key email threads)
* Meeting Cadence
* Outreach Cadence

### Installation Evaluation

Assessment of the environment where the customer's GitLab instance is hosted

* Self-Hosted Assessment
  * Health Checks
  * Architecture diagrams
  * Machine specifications
  * GitLab configuration files


* GitLab.com(SaaS) Assessment
  * Runners Evaluation
    * Private On-Prem Runners

  * CI Minutes Utilization
    * CI usage trends(i.e daily, weekly, monthly etc.)